(require 'package)
(add-to-list 'package-archives
             '("tromey" . "http://tromey.com/elpa/") t)
(add-to-list 'package-archives
             '("melpa". "https://melpa.org/packages/") t)

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(c-basic-offset 4)
 '(coffee-tab-width 2)
 '(column-number-mode t)
 '(global-auto-revert-mode t)
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(package-selected-packages
   (quote
    (intero gnu-elpa-keyring-update smex rainbow-delimiters projectile)))
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;Set up ido everywhere
(require 'ido)
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode t)

;; Install Intero (for haskell)
(package-install 'intero)
(add-hook 'haskell-mode-hook 'intero-mode)

;; Install other utility packages
(package-install 'gnu-elpa-keyring-update)
(package-install 'smex)
(package-install 'rainbow-delimiters)
(package-install 'projectile)

;Set the title according to the file name
(add-hook 'window-configuration-change-hook
          (lambda ()
            (setq frame-title-format
                  (concat
                   (replace-regexp-in-string
                    (concat "/home/" user-login-name) ; pattern
                    "~"                               ; replacement
                    (or buffer-file-name "%b")        ; string to replace
                    )
                   "@" system-name

                   )
                  )
            )
          )

;Set the default window height and width
(if (window-system)
    (set-frame-height (selected-frame) 33)
)

;Turn off the toolbar
(tool-bar-mode -1)

; Don't use tabs for indenting
(setq indent-tabs-mode nil)


;Enter tcl mode for tcl test files as well
(setq auto-mode-alist
      (cons '("\\.tcl.test$" . tcl-mode) auto-mode-alist))

;Enter xml mode for x3d test files
(setq auto-mode-alist
      (cons '("\\.x3d$" . xml-mode) auto-mode-alist))

;Perl mode for .t test files
(setq auto-mode-alist
      (cons '("\\.t$" . perl-mode) auto-mode-alist))

;Matlab mode
;(add-to-list 'load-path "~/elisp/matlab-emacs/")
;(load-library "matlab-load")

;; When I finally install cedet, uncomment the next line
;; (matlab-cedet-setup)

;Mathematica package mode
(autoload 'mma-mode "/home/eric/bin/emacs-mathematica-package-mode-mma.el" "Mathematica package file mode" t)
(setq auto-mode-alist
      (cons '("\\.m\\'" . mma-mode) auto-mode-alist))


;Add key binding for C-Tab to etags completion
(global-set-key (kbd "<C-tab>") 'complete-symbol)


;; Unfill region command - join all paragraphs into a single line
(defun unfill-region (beg end)
      "Unfill the region, joining text paragraphs into a single
    logical line.  This is useful, e.g., for use with
    `visual-line-mode'."
      (interactive "*r")
      (let ((fill-column (point-max)))
        (fill-region beg end)))

; Add 3rd party .el files located in home directory
(add-to-list 'load-path "~/elisp")

; Add elm backend to company-mode completion
(setq company-backends '(company-files company-elm))

; Load haml mode
; from https://github.com/nex3/haml-mode
;(require 'haml-mode)

; Turn off tabs in haml mode (haml uses only spaces for indentation)
;(add-hook 'haml-mode-hook
;               (lambda ()
;                 (setq indent-tabs-mode nil)
;                 (define-key haml-mode-map "\C-m" 'newline-and-indent)))

; Put all automatic emacs backups in the ~/.saves directory
(if (file-directory-p "~/.saves")
    (setq
     backup-by-copying t      ; don't clobber symlinks
     backup-directory-alist
     '(("." . "~/.saves"))    ; don't litter my fs tree
     delete-old-versions t
     kept-new-versions 2
     kept-old-versions 1
     version-control t)       ; use versioned backups
  (message "Backup directory does not exist: ~/.saves"))

;; This hack fixes indentation for C++11's "enum class" in Emacs.
;; http://stackoverflow.com/questions/6497374/emacs-cc-mode-indentation-problem-with-c0x-enum-class/6550361#6550361

(defun inside-class-enum-p (pos)
  "Checks if POS is within the braces of a C++ \"enum class\"."
  (ignore-errors
    (save-excursion
      (goto-char pos)
      (up-list -1)
      (backward-sexp 1)
      (looking-back "enum[ \t]+class[ \t]+[^}]*"))))

(defun align-enum-class (langelem)
  (if (inside-class-enum-p (c-langelem-pos langelem))
      0
    (c-lineup-topmost-intro-cont langelem)))

(defun align-enum-class-closing-brace (langelem)
  (if (inside-class-enum-p (c-langelem-pos langelem))
      '-
    '+))

(defun fix-enum-class ()
  "Setup `c++-mode' to better handle \"class enum\"."
  (add-to-list 'c-offsets-alist '(topmost-intro-cont . align-enum-class))
  (add-to-list 'c-offsets-alist
               '(statement-cont . align-enum-class-closing-brace)))

(add-hook 'c++-mode-hook 'fix-enum-class)


; Functions for loading split-up emacs files
; From: http://stackoverflow.com/a/2079146/309334
(defconst user-init-dir
  (cond ((boundp 'user-emacs-directory)
         user-emacs-directory)
        ((boundp 'user-init-directory)
         user-init-directory)
        (t "~/.emacs.d/")))


(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory"
  (load-file (expand-file-name file user-init-dir)))
