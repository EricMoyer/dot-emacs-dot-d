My .emacs.d
===========

Use this if you want to set up your emacs exactly like mine.

I borrowed heavily from the setup with _Clojure for the Brave and True_
- because I've been using emacs for almost 25 years but never got
around to really customizing it.